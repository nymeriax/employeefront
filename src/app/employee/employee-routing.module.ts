import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FinderComponent } from './components/finder/finder.component';
import { SaveCardComponent } from './components/save-card/save-card.component';
import { MainComponent } from './pages/main/main.component';

const routes: Routes = [
  {
    path : "",
    component : MainComponent
  },
  {
    path: "add",
    component: SaveCardComponent
  },
  {
    path: "finder",
    component: FinderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
