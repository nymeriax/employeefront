import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Person } from '../../interfaces/response';
import { EditService } from '../../services/edit.service';

@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.css']
})
export class EditCardComponent implements OnInit {

  @Input() data!: Person ;

  flag:string = "";

  subcription!:Subscription;

  constructor(private close:EditService) { }

  ngOnInit(): void {
  }

  closePanel(){
    this.close.emitClosedPanel(false);
  }

  sendData(){
    this.close.emitClosedPanel(this.data);
    console.log(this.data)
  }

}
