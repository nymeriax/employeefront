import { Component, OnInit } from '@angular/core';
import { Person } from '../../interfaces/response';
import { EmployeeService } from '../../services/employee.service';

const ELEMENT_DATA: Person[] = [
	{
		id: "1",
		firstLastName: "LEWAN",
		secondLastName: "MAN",
		firstName: "DOWSKI",
		otherName: "LONE",
		countryOfEmployment: "COLOMBIA",
		idType: "CE",
		idNumber: "1234",
		email: "DOWSKI.LEWAN@cidenet.com.co",
		entryDate: "2022-08-03",
		area: "RH",
		status: "ACTIVO",
		registrationDate: null,
		editionDate: null
	}

];

@Component({
  selector: 'app-finder',
  templateUrl: './finder.component.html',
  styleUrls: ['./finder.component.css']
})
export class FinderComponent implements OnInit {

  constructor(private employeeService:EmployeeService) { }

  name: String = "";
  dataSource !:Person[];


  
	displayedColumns: string[] = [
		"id",
		"firstLastName",
		"secondLastName",
		"firstName",
		"otherName",
		"countryOfEmployment",
		"idType",
		"idNumber",
		"email",
		"entryDate",
		"area",
		"status"
	];

  ngOnInit(): void {
  }

  find(){
    this.employeeService.findByName(`?firstName=${this.name}`).subscribe(
      r => {
        console.log("datos del filtrado",r);
        this.dataSource = r.content;
      }
    )
  }

}
