import { Component, OnInit } from '@angular/core';
import { Person } from '../../interfaces/response';
import { EmployeeService } from '../../services/employee.service';


@Component({
  selector: 'app-save-card',
  templateUrl: './save-card.component.html',
  styleUrls: ['./save-card.component.css']
})
export class SaveCardComponent implements OnInit {

  constructor(private employeeService: EmployeeService) { }


  data:Person = {
    id: '',
    firstLastName: '',
    secondLastName: '',
    firstName: '',
    otherName: '',
    countryOfEmployment: '',
    idType: '',
    idNumber: '',
    email: '',
    entryDate: '',
    area: '',
    status: ''
  }

  ngOnInit(): void {
  }

  save(){
    this.employeeService.saveEmployee(this.data).subscribe(
      r => {
        console.log("save", r);
        this.data = {id: '',
        firstLastName: '',
        secondLastName: '',
        firstName: '',
        otherName: '',
        countryOfEmployment: '',
        idType: '',
        idNumber: '',
        email: '',
        entryDate: '',
        area: '',
        status: ''}
      }
    )
  }

}
