import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Person } from '../../interfaces/response';
import { EditService } from '../../services/edit.service';
import { EmployeeService } from '../../services/employee.service';



const ELEMENT_DATA: Person[] = [
	{
		id: "1",
		firstLastName: "LEWAN",
		secondLastName: "MAN",
		firstName: "DOWSKI",
		otherName: "LONE",
		countryOfEmployment: "COLOMBIA",
		idType: "CE",
		idNumber: "1234",
		email: "DOWSKI.LEWAN@cidenet.com.co",
		entryDate: "2022-08-03",
		area: "RH",
		status: "ACTIVO",
		registrationDate: null,
		editionDate: null
	},
	{
		id: "2",
		firstLastName: "LEWANDOWSKI",
		secondLastName: "BAY",
		firstName: "LEWY",
		otherName: "LONE",
		countryOfEmployment: "COLOMBIA",
		idType: "CE",
		idNumber: "12345",
		email: "LEWY.LEWANDOWSKI@cidenet.com.co",
		entryDate: "2022-08-03",
		area: "RH",
		status: "ACTIVO",
		registrationDate: null,
		editionDate: null
	},
	{
		id: "3",
		firstLastName: "DOSANTOS",
		secondLastName: "RONALDO",
		firstName: "CRISTIANO",
		otherName: "BAY",
		countryOfEmployment: "EEUU",
		idType: "CE",
		idNumber: "123456",
		email: "CRISTIANO.DOSANTOS@cidenet.com.us",
		entryDate: "2022-08-03",
		area: "RH",
		status: "ACTIVO",
		registrationDate: null,
		editionDate: null
	},
	{
		id: "4",
		firstLastName: "MUSK",
		secondLastName: "OR",
		firstName: "ELON",
		otherName: "BFAY",
		countryOfEmployment: "EEUU",
		idType: "CE",
		idNumber: "1234567",
		email: "ELON.MUSK@cidenet.com.us",
		entryDate: "2022-08-03",
		area: "RH",
		status: "ACTIVO",
		registrationDate: null,
		editionDate: null
	},

	{
		id: "15",
		firstLastName: "THOR",
		secondLastName: "ASGARD",
		firstName: "SLIGER",
		otherName: "PRESTON",
		countryOfEmployment: "COLOMBIA",
		idType: "CE",
		idNumber: "1234567891A",
		email: "SLIGER.THOR@cidenet.com.co",
		entryDate: "2020-12-31",
		area: "PUR",
		status: "ACTIVO",
		registrationDate: null,
		editionDate: null
	}

];
@Component({
	selector: 'app-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

	displayedColumns: string[] = [
		"id",
		"firstLastName",
		"secondLastName",
		"firstName",
		"otherName",
		"countryOfEmployment",
		"idType",
		"idNumber",
		"email",
		"entryDate",
		"area",
		"status",
		"Edit"
	];
	dataSource !: Person[];
	

	auxData!:Person ;
	subcription!:Subscription;

	isEdit: boolean = false;
	loading: boolean = true;
	constructor(
		private closeEdit:EditService,
		private employeService: EmployeeService) { }

	ngOnInit(): void {
		this.subcription =  this.closeEdit.closeEditPanelObservable().subscribe(
			d => {
				if (d) {
					console.log("hacer request", d);
					this.employeService.UpdateEmployee(d).subscribe(
						r =>{
							console.log("saved", r);
							this.getAllData();
						}
					)
					
				}
				this.isEdit = false;
				
			}
		)

		this.getAllData();

		


	}

	edit(event: any) {
		this.isEdit = true;
		this.auxData = {...event}

	}

	getAllData(){
		this.employeService.getAllEmployee().subscribe(
			r => {
				this.dataSource = r.content;
				this.loading = false;
			},
			err => {
				this.loading = false;
			}
		);
	}

}
