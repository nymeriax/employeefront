export interface EmployeeResponse {
	content: Person[];
	pageable: Pageable;
	totalPages: number;
	totalElements: number;
	last: boolean;
	size: number;
	number: number;
	sort: Sort;
	numberOfElements: number;
	first: boolean;
	empty: boolean;
  }


export interface Person {
	id: string; // NOI
	firstLastName: string;
	secondLastName: string;
	firstName: string;
	otherName: string;
	countryOfEmployment: string;
	idType: string;
	idNumber: string;
	email: string; // NO
	entryDate: string; 
	area: string;
	status: string; // NO
	registrationDate?: any // NO;
	editionDate?: any;
}




interface Pageable {
  sort: Sort;
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  unpaged: boolean;
}

interface Sort {
  empty: boolean;
  unsorted: boolean;
  sorted: boolean;
}

