import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditService {

  closeEditPanel:Subject<any> = new Subject();

  constructor() { }

  closeEditPanelObservable(): Observable<any>{
    return this.closeEditPanel.asObservable();
  }

  emitClosedPanel(data:any){
    this.closeEditPanel.next(data);
  }
}
