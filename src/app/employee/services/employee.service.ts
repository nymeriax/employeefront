import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EmployeeResponse } from '../interfaces/response';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private apiUrl:string = environment.apiUrl;
  constructor(private http: HttpClient) { }


  getAllEmployee(): Observable<EmployeeResponse>{
    return this.http.get<EmployeeResponse>(this.apiUrl);
  }


  findByName(params:string): Observable<EmployeeResponse>{
    return this.http.get<EmployeeResponse>(`${this.apiUrl}${params}`)
  }

  saveEmployee(data:any){
    return this.http.post(this.apiUrl,data);
  }
  
  UpdateEmployee(data:any){
    return this.http.put(this.apiUrl,data);
  }


  
}
