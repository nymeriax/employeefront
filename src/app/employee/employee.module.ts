import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { MainComponent } from './pages/main/main.component';
import { MaterialModule } from '../material/material.module';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { SaveCardComponent } from './components/save-card/save-card.component';
import { FormsModule } from '@angular/forms';
import { FinderComponent } from './components/finder/finder.component';


@NgModule({
  declarations: [
    MainComponent,
    EditCardComponent,
    SaveCardComponent,
    FinderComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    MaterialModule,
    FormsModule
  ]
})
export class EmployeeModule { }
